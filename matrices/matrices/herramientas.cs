﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrices
{
    class herramientas
    {
        private string[] empleados;
        private int[,] sueldos;
        private int[] sueldostot;
        public static void Cargar()
        {
            empleados = new String[4];
            sueldos = new int[4, 3];
            for (int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Ingrese el nombre del operario " + (f + 1) + ": ");
                empleados[f] = Console.ReadLine();
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    Console.Write("Ingrese sueldo " + (c + 1) + ": ");
                    string linea;
                    linea = Console.ReadLine();
                    sueldos[f, c] = int.Parse(linea);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            sueldostot = new int[4];
            for (int f = 0; f < sueldos.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    suma = suma + sueldos[f, c];
                }
                sueldostot[f] = suma;
            }
        }

        public void ImprimirTotalPagado()
        {
            Console.WriteLine("Total de sueldos pagados por Operario.");
            for (int f = 0; f < sueldostot.Length; f++)
            {
                Console.WriteLine(empleados[f] + " - " + sueldostot[f]);
            }
        }

        public void EmpleadoMayorSueldo()
        {
            int may = sueldostot[0];
            string nom = empleados[0];
            for (int f = 0; f < sueldostot.Length; f++)
            {
                if (sueldostot[f] > may)
                {
                    may = sueldostot[f];
                    nom = empleados[f];
                }
            }
            Console.WriteLine("El operario con mayor sueldo es " + nom + " que tiene un sueldo de " + may);
        }

        
     
        public static void matriz2()
        {
            int F = 0, C = 0, N = 0, MI = 0, FI = 0;
            // MATRIZ EN BLANCO
            Console.Write("TAMAÑO DE LA MATRIZ IMPAR:");
            N = int.Parse(Console.ReadLine());
            N = (N % 2 == 0 ? N + 1 : N);
            string[,] MAT = new string[N + 1, N + 1];
            // MATRIZ EN BLANCO
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = " ";
                    // UN ESPACIO EN BLANCO
                }
            }
            // FORMAMOS LA LETRA
            MI = N / 2 + 1;

            for (F = 1; F <= N; F++)
            {
                MAT[F, 1] = "A";
                MAT[MI, F] = "A";
                MAT[1, F] = "A";
            }
            FI = MI;
            // PARA EMPEZAR DESDE FILA DE LA MITAD
            for (F = 1; F <= MI; F++)
            {
                MAT[F, N] = "A";
                MAT[FI, FI] = "A";
                FI = FI + 1;
            }
            // SALIDA
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    Console.SetCursorPosition(C, F + 1);
                    Console.Write(MAT[F, C]);
                }
            }
            Console.WriteLine();
           
        }
        public static void matriz3()
        {
            int F = 0;
            int C = 0;
            int N = 0;
            int MAY = 0;
            int MEN = 0;
            string linea;
            // INGRESO
            // PARA COMODIDAD GENERAMOS VALORES PARA LA MATRIZ
            Console.Write("\nTAMAÑO DE LA MATRIZ:");
            Random rnd = new Random();
            linea = Console.ReadLine();
            N = int.Parse(linea);
            int[,] MAT = new int[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 99);
                    Console.SetCursorPosition(C * 4, F + 1);
                    Console.Write(MAT[F, C]);
                }
            }
            // PROCESO

            MAY = MAT[1, 1];
            MEN = MAT[1, 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    if ((MAT[F, C] > MAY))
                        MAY = MAT[F, C];
                    if ((MAT[F, C] < MEN))
                        MEN = MAT[F, C];
                }
            }
            // SALIDA
            Console.WriteLine();
            Console.WriteLine("\nNÚMERO MAYOR ES: " + MAY);
            Console.WriteLine("\nNÚMERO MENOR ES: " + MEN);
            
        }
        public static void matriz4()
        {

            int F = 0;
            int C = 0;
            int I = 0;
            int K = 0;
            int N = 0;
            int AUX = 0;
            string cadena;
            // INGRESO
            // PARA COMODIDAD GENERAMOS VALORES PARA LA MATRIZ
            Console.Write("\nTAMAÑO DE LA MATRIZ: ");
            cadena = Console.ReadLine();
            Random rnd = new Random();
            N = int.Parse(cadena);
            int[,] MAT = new int[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 100);
                    Console.SetCursorPosition(C * 4, F + 1);
                    Console.Write(MAT[F, C]);
                }
            }
            // PROCESO
            for (F = 1; F <= N; F++)
            {

                for (C = 1; C <= N; C++)
                {
                    for (I = 1; I <= N; I++)
                    {
                        for (K = 1; K <= N; K++)
                        {
                            if ((MAT[F, C] < MAT[I, K]))
                            {
                                AUX = MAT[F, C];
                                MAT[F, C] = MAT[I, K];
                                MAT[I, K] = AUX;
                            }
                        }
                    }
                }
            }
            // SALIDA
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    Console.SetCursorPosition(C * 4, F + 10);
                    Console.Write(MAT[F, C]);
                }
            }
            Console.WriteLine();
            
        }
        public static void matriz5y6()
        {
            int F = 0;
            int C = 0;
            int N = 0;
            int MAY = 0;
            int MEN = 0;
            string linea;
            // INGRESO
            // PARA COMODIDAD GENERAMOS VALORES PARA LA MATRIZ
            Console.Write("\nTAMAÑO DE LA MATRIZ:");
            Random rnd = new Random();
            linea = Console.ReadLine();
            N = int.Parse(linea);
            int[,] MAT = new int[N + 1, N + 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    MAT[F, C] = rnd.Next(0, 99);
                    Console.SetCursorPosition(C * 4, F + 1);
                    Console.Write(MAT[F, C]);
                }
            }
            // PROCESO

            MAY = MAT[1, 1];
            MEN = MAT[1, 1];
            for (F = 1; F <= N; F++)
            {
                for (C = 1; C <= N; C++)
                {
                    if ((MAT[F, C] > MAY))
                        MAY = MAT[F, C];
                    if ((MAT[F, C] < MEN))
                        MEN = MAT[F, C];
                }
            }
            // SALIDA
            Console.WriteLine();
            Console.WriteLine("\nNÚMERO MAYOR ES: " + MAY);
            Console.WriteLine("\nNÚMERO MENOR ES: " + MEN);

        }

    }
}
